package features.environment;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

public class Hooks {

    private static FirefoxDriver webDriver;


    @Before
    public static WebDriver openBrowser() {

        System.setProperty("webdriver.gecko.driver", "C:\\geckodriver-v0.29.1-win64\\geckodriver.exe");
        webDriver = new FirefoxDriver();
        WebDriverManager.firefoxdriver().setup();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("http://www.automationpractice.com");

        return webDriver;
    }

    @After
    public static void quitBrowser() {
        if (webDriver != null) {
            webDriver.quit();
        }

    }
}









