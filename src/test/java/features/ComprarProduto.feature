#language: pt
@funcionais
Funcionalidade: ComprarProduto

  Contexto:
    Dado que esteja no site


  Cenário: Selecionar um produto na página
    Dado   usuário escolhe um produto
      | produto                     |
      | Printed Chiffon Dress       |
      | Faded Short Sleeve T-shirts |
      | Blouse                      |
      | Printed Dress               |
    Quando procede para o checkout
    Então  valido os dados do pedido
      | amount  | name owner       | bank |
      | $119.51 | Pradeep Macharla | RTP  |
