package features.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class ProdutoPage extends BasePage {

    //Locators
    private By product_1 = By.xpath("(//a[contains(@title, \"Printed Chiffon Dress\")])[2]");
    private By product_2 = By.xpath("(//a[contains(@title, \"Faded Short Sleeve T-shirts\")])[2]");
    private By product_3 = By.xpath("(//a[contains(@title, \"Blouse\")])[3]");
    private By product_4 = By.xpath("(//a[contains(@title, \"Printed Dress\")])[2]");
    private By add_cart = By.name("Submit");
    private By logo = By.xpath("//img[contains(@alt, \"My Store\")]");
    private By color_green = By.id("color_15");
    private By size = By.id("group_1");
    private By color_blue = By.id("color_14");
    private By quantity = By.id("quantity_wanted");
    private By closeWindow = By.xpath("//span[contains(@title, \"Close window\")]");

    public ProdutoPage(WebDriver driver) {
        super(driver);
    }

    public void clickProductAndAddCart(String product) {

        switch (product) {
            case "Printed Chiffon Dress":
                this.click(product_1);
                this.click(color_green);
                this.selectComboBox(size,"2");
                clickAddCart();
                this.click(closeWindow);
                this.click(logo);
                break;
            case "Faded Short Sleeve T-shirts":
                this.click(product_2);
                this.click(color_blue);
                clickAddCart();
                this.click(closeWindow);
                this.click(logo);
                break;
            case "Blouse":
                this.click(product_3);
                this.writeText(quantity, "2");
                clickAddCart();
                this.click(closeWindow);
                this.click(logo);
                break;
            case "Printed Dress":
                this.click(product_4);
                clickAddCart();
                this.click(closeWindow);
                this.click(logo);
                break;
        }

    }

        public void clickAddCart() {
            this.click(add_cart);
        }


}

