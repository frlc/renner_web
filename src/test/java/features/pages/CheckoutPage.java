package features.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckoutPage extends BasePage{
    //Locators
    private By proceedToCheckout = By.linkText("Proceed to checkout");
    private By proceedToCheckout_addreses_and_shipping = By.name("processAddress");
    private By checkBox = By.id("uniform-cgv");
    private By processCarrier = By.name("processCarrier");
    private By payByBankWire = By.className("bankwire");
    private By confirmMyOrder = By.cssSelector(".cart_navigation .button-medium");
    private By cart = By.xpath("//a[contains(@title, \"View my shopping cart\")]");


    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    public void clickProceedToCheckout() {
        this.clickWithOutVisibility(proceedToCheckout);
    }

    public void clickProceedToCheckoutAddressAndShiping() {
        this.clickWithOutVisibility(proceedToCheckout_addreses_and_shipping);
    }

    public void clickCheckboxCheckout() {
        this.click(checkBox);
    }

    public void clickProcessCarrier() {
        this.click(processCarrier);
    }

    public void clickPayByBankWire() {
        this.click(payByBankWire);
    }

    public void clickConfirmMyOrder() {
        this.click(confirmMyOrder);
    }

    public void clickCart() {
        this.click(cart);
    }

}
