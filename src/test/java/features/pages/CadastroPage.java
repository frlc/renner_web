package features.pages;

import common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CadastroPage extends BasePage {
    public CadastroPage(WebDriver driver) {
        super(driver);
    }

    private By email_create = By.id("email_create");
    private By submit_create = By.id("SubmitCreate");
    private By id_gender_1 = By.id("id_gender1");
    private By customer_firstname = By.id("customer_firstname");
    private By customer_lastname = By.id("customer_lastname");
    private By email = By.id("email");
    private By passwd = By.id("passwd");
    private By days = By.id("days");
    private By months = By.id("months");
    private By years = By.id("years");
    private By firstname = By.id("firstname");
    private By lastname = By.id("lastname");
    private By company = By.id("company");
    private By address1 = By.id("address1");
    private By address2 = By.id("address2");
    private By city = By.id("city");
    private By id_state = By.id("id_state");
    private By postcode = By.id("postcode");
    private By id_country = By.id("id_country");
    private By other = By.id("other");
    private By phone = By.id("phone");
    private By phone_mobile = By.id("phone_mobile");
    private By alias = By.id("alias");
    private By submitAccount = By.id("submitAccount");
    private By processAddress = By.name("processAddress");


    public void setEmailAdress(String text) {
        this.writeText(email_create, text);
    }

    public void clickSubmitCreateButton() {
        this.getElement(submit_create).click();
    }

    public WebElement getRadioButton() {
        return this.getElement(id_gender_1);
    }

    public void setFirstCustomerName(String text) {
        this.writeText(customer_firstname, text);
    }

    public void setLastCustomerName(String text) {
        this.writeText(customer_lastname, text);
    }

    public void setPasswd(String text) {
        this.writeText(passwd, text);
    }

    public void comboDay(String value) {
        this.selectComboBox(days, value);
    }

    public void comboMonth(String value) {
        this.selectComboBox(months, value);
    }

    public void comboYear(String value) {
        this.selectComboBox(years, value);
    }

    public void setAdress(String value) {
        this.writeText(address1, value);
    }

    public void setCity(String value) {
        this.writeText(city, value);
    }

    public void setcomboState(String value) {
        this.selectComboBox(id_state, value);
    }

    public void setPostalCode(String value) {
        this.writeText(postcode, value);
    }

    public void setComboCountry(String value) {
        this.selectComboBox(id_country, value);
    }

    public void setMobilePhone(String value) {
        this.writeText(phone_mobile, value);
    }

    public void clickRegisterButton() {
        this.click(submitAccount);
    }

    public void setForm(){
        Utils utils = new Utils();

        this.getRadioButton().click();
        this.setFirstCustomerName(utils.randomName());
        this.setLastCustomerName(utils.randomName());
        this.setPasswd(utils.randomName());
        this.comboDay("31");
        this.comboMonth("12");
        this.comboYear("1980");
        this.setAdress("801 Tom Martin Dr.Birmingham, AL 35211");
        this.setCity("Birmingham");
        this.setcomboState("1");
        this.setPostalCode("54400");
        this.setComboCountry("21");
        this.setMobilePhone("81998374854");



    }

}

