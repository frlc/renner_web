package features.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PedidoPage extends BasePage{
    //Locators
    private By title_page_order = By.xpath("//h1[contains(@class, 'page-heading')]");
    private By total_order = By.xpath("//span[@class='price']");
    private By name_owner_order = By.xpath("(//strong)[9]");
    private By name_bank_order = By.xpath("(//strong)[11]");


    public PedidoPage(WebDriver driver) {
        super(driver);
    }

    public String getTotal() {

        return this.readText(total_order);
    }

    public String getNameOwner() {

        return this.readText(name_owner_order);
    }

    public String getNameBank() {

        return this.readText(name_bank_order);
    }

    public void waitVisibilityPagePedido() {

        this.waitVisibility(title_page_order);
    }
}
