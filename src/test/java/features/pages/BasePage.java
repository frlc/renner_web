package features.pages;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    private static final int TIMEOUT = 10;
    private static final int POLLING = 100;

    protected WebDriver driver;
    private WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, TIMEOUT, POLLING);
        PageFactory.initElements(driver, this);
    }

    //Wait Wrapper Method
    public void waitVisibility(By locator) {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    //Click Method
    public void click(By locator) {
        waitVisibility(locator);
        driver.findElement(locator).click();
    }

    //Click Method
    public void clickWithOutVisibility(By locator) {
        driver.findElement(locator).click();
    }

    //Write Text
    public void writeText(By locator, String text) {
        waitVisibility(locator);
        driver.findElement(locator).clear();
        driver.findElement(locator).sendKeys(text);
    }

    //Read Text
    public String readText(By locator) {
        waitVisibility(locator);
        return driver.findElement(locator).getText();
    }

    public WebElement getElement(By locator) {
        return driver.findElement(locator);
    }

    public void selectComboBox(By locator, String value) {
        new Select(driver.findElement(locator)).selectByValue(value);
    }

    public void clickWithJavascript(By locator) {
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);

    }
}
