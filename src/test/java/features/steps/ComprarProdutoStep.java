package features.steps;

import common.Utils;
import cucumber.api.DataTable;
import cucumber.api.java.en.When;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import features.environment.Hooks;
import features.pages.CadastroPage;
import features.pages.CheckoutPage;
import features.pages.PedidoPage;
import features.pages.ProdutoPage;
import org.openqa.selenium.WebDriver;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class ComprarProdutoStep {

    private static WebDriver driver;
    private ProdutoPage produtoPage;
    private CheckoutPage checkoutPage;
    private CadastroPage cadastroPage;
    private Utils utils;
    private PedidoPage pedidoPage;


    @Dado("^que esteja no site$")
    public void queEstejaNoSite() throws Throwable {
        driver = Hooks.openBrowser();
    }

    @Dado("^usuário escolhe um produto$")
    public void usuário_escolhe_um_produto(DataTable table) {
        produtoPage = new ProdutoPage(driver);
        List<String> rows = table.asList(String.class);
        for (String row : rows) {
            produtoPage.clickProductAndAddCart(row);
        }

    }


    @When("^procede para o checkout$")
    public void procede_para_o_checkout() {
        checkoutPage = new CheckoutPage(driver);
        cadastroPage = new CadastroPage(driver);
        utils = new Utils();
        checkoutPage.clickCart();
        checkoutPage.clickProceedToCheckout();
        cadastroPage.setEmailAdress(utils.randomEmail());
        cadastroPage.clickSubmitCreateButton();
        cadastroPage.setForm();
        cadastroPage.clickRegisterButton();
        checkoutPage.clickProceedToCheckoutAddressAndShiping();
        checkoutPage.clickCheckboxCheckout();
        checkoutPage.clickProcessCarrier();
        checkoutPage.clickPayByBankWire();
        checkoutPage.clickConfirmMyOrder();

    }


    @Então("^valido os dados do pedido$")
    public void validoOsDadosDoPedido(DataTable table) {
        pedidoPage = new PedidoPage(driver);
        pedidoPage.waitVisibilityPagePedido();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> columns : rows) {
            assertThat(pedidoPage.getTotal(), equalTo(columns.get("amount")));
            assertThat(pedidoPage.getNameOwner(), equalTo(columns.get("name owner")));
            assertThat(pedidoPage.getNameBank(), equalTo(columns.get("bank")));
        }
        Hooks.quitBrowser();

    }
}