$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ComprarProduto.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 3,
  "name": "ComprarProduto",
  "description": "",
  "id": "comprarproduto",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 2,
      "name": "@funcionais"
    }
  ]
});
formatter.background({
  "line": 5,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 6,
  "name": "que esteja no site",
  "keyword": "Dado "
});
formatter.match({
  "location": "ComprarProdutoStep.queEstejaNoSite()"
});
formatter.result({
  "duration": 16448874500,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Selecionar um produto na página",
  "description": "",
  "id": "comprarproduto;selecionar-um-produto-na-página",
  "type": "scenario",
  "keyword": "Cenário"
});
formatter.step({
  "line": 10,
  "name": "usuário escolhe um produto",
  "rows": [
    {
      "cells": [
        "produto"
      ],
      "line": 11
    },
    {
      "cells": [
        "Printed Chiffon Dress"
      ],
      "line": 12
    },
    {
      "cells": [
        "Faded Short Sleeve T-shirts"
      ],
      "line": 13
    },
    {
      "cells": [
        "Blouse"
      ],
      "line": 14
    },
    {
      "cells": [
        "Printed Dress"
      ],
      "line": 15
    }
  ],
  "keyword": "Dado "
});
formatter.step({
  "line": 16,
  "name": "procede para o checkout",
  "keyword": "Quando "
});
formatter.step({
  "line": 17,
  "name": "valido os dados do pedido",
  "rows": [
    {
      "cells": [
        "amount",
        "name owner",
        "bank"
      ],
      "line": 18
    },
    {
      "cells": [
        "$119.51",
        "Pradeep Macharla",
        "RTP"
      ],
      "line": 19
    }
  ],
  "keyword": "Então "
});
formatter.match({
  "location": "ComprarProdutoStep.usuário_escolhe_um_produto(DataTable)"
});
formatter.result({
  "duration": 79179854800,
  "status": "passed"
});
formatter.match({
  "location": "ComprarProdutoStep.procede_para_o_checkout()"
});
formatter.result({
  "duration": 63776072400,
  "status": "passed"
});
formatter.match({
  "location": "ComprarProdutoStep.validoOsDadosDoPedido(DataTable)"
});
formatter.result({
  "duration": 6580462300,
  "status": "passed"
});
});